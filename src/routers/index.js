import accountingRouter from "./Accounting.router.js";
import creditCardRouter from "./CreditCard.router.js";
import bankInfoRouter from "./BankInfo.router.js";

export default { accountingRouter, creditCardRouter, bankInfoRouter };
