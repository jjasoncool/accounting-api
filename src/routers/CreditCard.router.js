import Router from "koa-router";
import { CreditCard } from "../models/index.js";

const router = new Router();

// for create credit card
router.post("/creditCard", async (ctx) => {
  const creditCard = new CreditCard(ctx.request.body);

  await creditCard.save();

  ctx.body = creditCard;
});

export default router;
