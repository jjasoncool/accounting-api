import Router from "koa-router";
import { BankInfo } from "../models/index.js";

const router = new Router();

// for create credit card
router.post("/bankInfo", async (ctx) => {
  const bankInfo = new BankInfo(ctx.request.body);

  await bankInfo.save();

  ctx.body = bankInfo;
});

export default router;
