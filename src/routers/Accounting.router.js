import Router from "koa-router";
import { Accounting } from "../models/index.js";

const router = new Router();

router
  .post("/accounting", async (ctx) => {
    const { date, amount, payment, creditCardID } = ctx.request.body;

    console.log(ctx.request.body);

    const accounting = new Accounting({
      date: new Date(date),
      amount,
      payment,
      creditCardID,
    });
    // console.log(ctx.request.body.title)

    await accounting.save();
    // ？
    // await Article.create({
    //   title: ctx.request.body
    // });

    ctx.body = accounting;
  })
  .put("/accounting/:id", async (ctx) => {})
  .get("/accounting/:id", async (ctx) => {
    const joinedAccounting = await Accounting.findOne({ _id: ctx.params.id }).populate(
      "creditCardID"
    );

    ctx.body = joinedAccounting;
  })
  .delete("/accounting/:id", (ctx) => {
    /* ... */
  });

export default router;
