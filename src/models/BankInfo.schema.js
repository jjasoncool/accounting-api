import mongoose from 'mongoose';
const { Schema } = mongoose;

const bankInfoSchema = new Schema({
  bankCode: String,
  bankName: String,
});

// 1:asc -1:desc
bankInfoSchema.index({ bankCode: 1 }, { unique: true })

export default bankInfoSchema;
