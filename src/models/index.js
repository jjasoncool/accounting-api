import mongoose from "mongoose";
import accountingSchema from "./Accounting.schema.js";
import creditCardSchema from "./CreditCard.schema.js";
import bankInfoSchema from "./BankInfo.schema.js";

const { Mongoose } = mongoose;

// move to config
const MONGO_WRITE_URI =
  "mongodb://general-user:password@mongodb-primary:27017,mongodb-secondary:27018,mongodb-arbiter:27019/demo?authSource=admin&replicaSet=rs0";
const commonDbSettings = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  autoIndex: true,
  useUnifiedTopology: true,
  maxPoolSize: 5,
};

const db = new Mongoose();

db.set("debug", true);
// db collection name
db.model("Accounting", accountingSchema);
db.model("CreditCard", creditCardSchema);
db.model("BankInfo", bankInfoSchema);

const connection = db.createConnection(MONGO_WRITE_URI, commonDbSettings);

// export to other files import
export const Accounting = connection.model("Accounting");
export const CreditCard = connection.model("CreditCard");
export const BankInfo = connection.model("BankInfo");
