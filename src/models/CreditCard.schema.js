import mongoose from 'mongoose';
const { Schema } = mongoose;

const creditCardSchema = new Schema({
  cardNumber: {
    type: String
  },
  bankCode: String,
});

export default creditCardSchema;
