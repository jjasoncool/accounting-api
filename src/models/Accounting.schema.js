import mongoose from 'mongoose';
const { Schema } = mongoose;

const accountingSchema = new Schema({
  date: Date,
  amount: Number,
  payment: String,
  creditCardID: { type: Schema.Types.ObjectId, ref: "CreditCard" },
});

export default accountingSchema;
