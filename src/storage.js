class StorageLayer {
    constructor() {
        this.someData = []
    }

    push(datum) {
        this.someData.push(datum)
    }
}

module.exports = new StorageLayer()