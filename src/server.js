// Koa
import Koa from 'koa';
import koaBody from "koa-body";
import routers from "./routers/index.js";

const app = new Koa();

app.use(koaBody());
app.use(...Object.values(routers).map(router => router.routes()));

app.listen(3000);
console.log('Server is up')
