# mongodb connection string: #

### primary ###
`mongodb://mongodb-primary:27017/?readPreference=primary`
### secondary ###
`mongodb://mongodb-secondary:27018/?readPreference=secondary`
### hosts setting (must)###
    /etc/hosts:
    127.0.0.1       mongodb-primary
    127.0.0.1       mongodb-secondary
    127.0.0.1       mongodb-arbiter
### replica ###


### create general-user: ###
- if password not set in docker then just type `mongo` to login
`use admin`
`db.createUser({ user: "general-user" , pwd: "password", roles: ["userAdminAnyDatabase", "dbAdminAnyDatabase", "readWriteAnyDatabase"]})`

### connect: ###
`mongodb://general-user:password@mongodb-primary:27017,mongodb-secondary:27018,mongodb-arbiter:27019/databasename?authSource=admin&replicaSet=rs0`
