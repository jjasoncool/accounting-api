/* eslint-disable @typescript-eslint/no-var-requires */
const mongoose = require('mongoose');
const { Mongoose, Schema, Types } = mongoose;

const blogSchema = new Schema({
  title: String,
});

MONGO_READ_URI =
  'mongodb://general-user:password@mongodb-primary:27017,mongodb-secondary:27018,mongodb-arbiter:27019/demo?authSource=admin&replicaSet=rs0';
MONGO_WRITE_URI =
  'mongodb://general-user:password@mongodb-primary:27017,mongodb-secondary:27018,mongodb-arbiter:27019/demo?authSource=admin&replicaSet=rs0';

const commonDbSettings = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  autoIndex: true,
  useUnifiedTopology: true,
  maxPoolSize: 5,
};

const dbInstances = {
  read: new Mongoose(),
  write: new Mongoose(),
};

dbInstances.write.set('debug', true);

dbInstances.read.model('Blog', blogSchema);
dbInstances.write.model('Blog', blogSchema);

const readConn = dbInstances.read.createConnection(
  MONGO_READ_URI,
  commonDbSettings,
);
const writeConn = dbInstances.write.createConnection(
  MONGO_WRITE_URI,
  commonDbSettings,
);

const BlogModels = {
  read: readConn.model('Blog'),
  write: writeConn.model('Blog'),
};

  BlogModels.write
  .create({
    // _id: '61609220fe0e1660b1451e36',
    title: 'hihi',
  })
  .then((data) => {
    console.log(data);

    // BlogModels.write
    //   .findByIdAndUpdate(
    //     {
    //       _id: '61609220fe0e1660b1451e36',
    //     },
    //     {
    //       title: '圭',
    //     },
    //     { new: true },
    //   )
    //   .then((data) => {
    //     console.log(data);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
    // BlogModels.write
    //   .findById('61609220fe0e1660b1451e36')
    //   .then((data) => {
    //     console.log(data);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
  })
  .catch((err) => {
    console.log(err);
  });
